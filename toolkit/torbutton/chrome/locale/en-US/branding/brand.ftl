# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor Browser
-brand-short-name = Tor Browser
-brand-full-name = Tor Browser
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor Browser
-vendor-short-name = Tor Project
# "Tor" is a trademark names, so should not be translated (not including the quote marks, which can be localized).
# "The Tor Project, Inc." is an organisation name.
trademarkInfo = “Tor” and the Tor logo are registered trademarks of The Tor Project, Inc.
